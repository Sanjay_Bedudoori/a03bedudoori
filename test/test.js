



// QUnit.test('Testing calculate function with many inputs', function (assert) {
//     assert.equal(calculate(1,2,3), 0.8240000000000001, 'Tested with three relatively small positive numbers');
//     // assert.equal(calculate(-1, -2,-3), -0.8240000000000001, 'Tested with two negative numbers. Any arguments less than 1 will be set to 1.');
    
//     // assert.throws(function () { calculate(-1,-2,-3); }, new Error ("Enter only positive numeric value"), 'raises an error');/The given argument is not a number/, 'Passing in a string correctly raises an Error');
// });

QUnit.test("Here's a test that should always pass", function (assert) {
    assert.ok(1 <= "3", "1<3 - the first agrument is 'truthy', so we pass!");
});

QUnit.test('Testing calculate function with several sets of inputs', function (assert) {
    assert.equal(calculate(5, 5, 5), 10.5, 'Tested with three relatively small positive numbers');
    // assert.equal(calculate(-5, -5,-5), 9.5, 'Tested with two negative numbers. Any arguments less than 1 will be set to 1.');
	assert.equal(calculate(3, 4, 0), 4.800000000000001, 'Tested with third argument as 0');

	assert.throws(function () { calculate(-3,-4,-5); }, /Enter only positive numeric value/, 'Passing in negative values throws an error');

     assert.throws(function () { calculate(0,0,0); }, /Enter only positive numeric value/, 'Passing all zeros');
    //     //throws( block                                    [, expected ] [, message ] ) 
     assert.throws(function () { calculate(null,null,null); }, /Enter only positive numeric value/, 'Passing in null  raises an Error');
    //     //throws( block                                    [, expected ] [, message ] ) 
    assert.throws(function () { calculate("Christine","Christine","Christine"); }, /Enter only positive numeric value/, 'Passing in a string correctly  an Error');

    

});
