var path = require("path");
var express = require("express");
var logger = require("morgan");
var bodyParser = require("body-parser");


var app = express();  // make express app
var http = require('http').Server(app);

// set up the view engine

app.use(express.static(path.join(__dirname, 'assets')));
app.set("views", path.resolve(__dirname, "views")); // path to views
app.set("view engine", "ejs"); // specify our view engine

// create an array to manage our entries

var entries = [];
app.locals.entries = entries;



// set up the http request logger
app.use(logger("dev"));
app.use(bodyParser.urlencoded({ extended: false }));

// manage our entries
// http GET (default and /new-entry)
app.get("/", function (request, response) {
  
  response.sendFile(path.join(__dirname + '/assets/index.html'));
});
app.get("/about", function (request, response) {
  response.sendFile(path.join(__dirname + '/assets/about.html'));
});
app.get("/contact", function (request, response) {
  response.sendFile(path.join(__dirname + '/assets/contact.html'));
 
});
app.get("/book", function (request, response) {
  response.sendFile(path.join(__dirname + '/assets/book.html'));
});
app.post("/contact", function (request, response) {
  var api_key = 'key-dc406f58c3738a46632b680510327a01';
  var domain = 'sandbox7c930802a19042d197937d65d11796bb.mailgun.org';
  var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
   
  var data = {
    from: request.body.name + '<postmaster@sandbox7c930802a19042d197937d65d11796bb.mailgun.org>',
    to: 'sanjaybedudoori@gmail.com',
    subject: "Message from " + request.body.email,
    text: request.body.message
  };
 
mailgun.messages().send(data, function (error, body) {
  console.log(body);
  if(error){
      
     response.redirect("/404");
     
  }
  else{
     response.send("<script>alert('Mail Sent'); location.href='/'; </script>") ;
     // response.redirect("/");
  }
});
 
});
app.get("/guestbook", function (request, response) {
  response.render("guestbook");

});

 app.get("/new-entry", function (request, response) {
  response.render("new-entry");
});

// set up the logger
// GETS
// POSTS
// http POST (INSERT)
app.post("/new-entry", function (request, response) {
  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.");
    return;
  }
  entries.push({
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  });
  response.redirect("/guestbook");
});
// 404
// 404
app.use(function (request, response) {
  response.status(404).render("404");
});





// Listen for an application request on1port 8081
http.listen(8081, function () {
  console.log('Application listening on http://127.0.0.1:8081/');
});