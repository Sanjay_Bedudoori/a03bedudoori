# A03 Cab booking app with guestbook and contact page that sends an email.

Application using Node, Express, BootStrap, EJS

## How to use

Open a command window in your c:\44563\A03Bedudoori folder.

Run npm install to install all the dependencies in the package.json file.

Run node server.js to start the server.  (Hit CTRL-C to stop.)

```
> npm install
> node server.js
```

Point your browser to `http://localhost:8081`. 
